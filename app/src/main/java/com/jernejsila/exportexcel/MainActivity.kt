package com.jernejsila.exportexcel

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.FileProvider
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val file = File(this.cacheDir, "export.xlsx")

        val workbook = XSSFWorkbook()

        val sheet = workbook.createSheet()

        sheet
            .createRow(0) // if row was already created we need to call .getRow(0)
            .createCell(0)
            .setCellValue("1")

        workbook.write(file.outputStream())

        val intent = Intent(Intent.ACTION_SEND).apply {
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            type = "application/vnd.ms-excel"
            putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(applicationContext, BuildConfig.APPLICATION_ID, file))
        }
        startActivity(intent)
    }
}
